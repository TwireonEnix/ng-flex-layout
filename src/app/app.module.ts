import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { AppComponent } from './app.component';
import { MainNavComponent } from './main-nav/main-nav.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutModule } from '@angular/cdk/layout';
import {
  MatToolbarModule,
  MatButtonModule,
  MatSidenavModule,
  MatIconModule,
  MatListModule,
  MatMenuModule,
  MatCardModule,
  MatTabsModule
} from '@angular/material';
import { FirstComponent } from './first/first.component';
import { AppRouting } from './app.routing';
import { BasicRowColumnComponent } from './components/basic-row-column/basic-row-column.component';
import { ReverseRowColumnComponent } from './components/reverse-row-column/reverse-row-column.component';
import { MainAndCrossAxisComponent } from './components/main-and-cross-axis/main-and-cross-axis.component';
import { JustifyingContentComponent } from './components/justifying-content/justifying-content.component';
import { GapAndWrapComponent } from './components/gap-and-wrap/gap-and-wrap.component';
import { ItemsAndAlignmentComponent } from './components/items-and-alignment/items-and-alignment.component';
import { HolyGrailComponent } from './components/holy-grail/holy-grail.component';

@NgModule({
  declarations: [
    AppComponent,
    MainNavComponent,
    FirstComponent,
    BasicRowColumnComponent,
    ReverseRowColumnComponent,
    MainAndCrossAxisComponent,
    JustifyingContentComponent,
    GapAndWrapComponent,
    ItemsAndAlignmentComponent,
    HolyGrailComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatMenuModule,
    FlexLayoutModule,
    AppRouting,
    MatCardModule,
    MatTabsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
