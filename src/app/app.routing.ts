import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainNavComponent } from './main-nav/main-nav.component';
import { FirstComponent } from './first/first.component';
import { JustifyingContentComponent } from './components/justifying-content/justifying-content.component';
import { GapAndWrapComponent } from './components/gap-and-wrap/gap-and-wrap.component';
import { ItemsAndAlignmentComponent } from './components/items-and-alignment/items-and-alignment.component';
import { HolyGrailComponent } from './components/holy-grail/holy-grail.component';

const appRoutes: Routes = [
  {
    path: '',
    component: MainNavComponent,
    children: [
      { path: 'basic-row', component: FirstComponent },
      { path: 'justifiying', component: JustifyingContentComponent },
      { path: 'gap-wrap', component: GapAndWrapComponent },
      { path: 'items-alignment', component: ItemsAndAlignmentComponent },
      { path: 'holy-grail', component: HolyGrailComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRouting {}
